<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// I
	'imprimir_documento_description' => 'Plugin pour obtenir une version imprimable des articles et des brèves... Pour l\'utiliser, il suffit d\'ajouter la balise <code>#IMPRIMIR_DOCUMENTO**</code> dans les squelettes sans oublier les deux astérisques (dans les articles et brèves)',
	'imprimir_documento_nom' => 'Imprimer document',
	'imprimir_documento_slogan' => 'Obtenir une version imprimable des articles et des brèves...',
);
