<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// I
	'imprimir_documento_description' => 'Versión para imprimir artículos y breves sin cabeceras, ni mens,... Para utilizarla coloca la baliza <code>#IMPRIMIR_DOCUMENTO**</code> donde quieras que aparezca el enlace (artíulos o breves). Los dos ** asteriscos son importantes, no los olvides.',
	'imprimir_documento_nom' => 'Imprimir documento',
	'imprimir_documento_slogan' => 'Imprimir artículos y breves sin cabeceras, ni mens,...',
);
