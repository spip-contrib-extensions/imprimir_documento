<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}




/**
 * @param $p
 * @return mixed
 */
function balise_IMPRIMIR_DOCUMENTO_dist($p){

	$_id_article = champ_sql('id_article', $p);
	$_id_breve = champ_sql('id_breve', $p);

	$p->code = "preparar_enlace_imprimir(preparar_enlace_imprimir_args(\$Pile[0],$_id_article,$_id_breve))";
	$p->statut = 'html';
	$p->interdire_scripts = false;

	return $p;
}


